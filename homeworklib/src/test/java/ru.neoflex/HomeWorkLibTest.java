package ru.neoflex;

import org.junit.Test;

import static org.junit.Assert.*;

public class HomeWorkLibTest {

    @Test
    public void stringRevers() {
        assertEquals("54321", HomeWorkLib.stringRevers("12345"));
    }
}