package ru.neoflex;

public class HomeWorkLib {
    public static void main(String[] args) {
        System.out.println(stringRevers("12345"));
    }
    public static String stringRevers(String param){
        if (param == null)
        return "Parametr is null";
        else
            return new StringBuilder(param).reverse().toString();


    }
}
