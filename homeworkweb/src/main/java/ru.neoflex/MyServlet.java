package ru.neoflex;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static ru.neoflex.HomeWorkLib.stringRevers;

public class MyServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {


        PrintWriter out = resp.getWriter();
        try {
            out.println("<html>");
            out.println("<h1>" + stringRevers(new StringBuilder(req.getParameter("input")).toString()) + "</h1>");
            out.println("</html>");
        } catch (NullPointerException e) {
            System.out.println("NPE");
        }

    }
}
