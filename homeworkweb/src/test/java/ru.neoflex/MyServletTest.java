package ru.neoflex;

import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class MyServletTest extends Mockito {
    @Test
    public void whenCallDoGetThenServletReturnReversString() throws ServletException, IOException {

        final MyServlet servlet = new MyServlet();

        final HttpServletRequest req = mock(HttpServletRequest.class);
        final HttpServletResponse resp = mock(HttpServletResponse.class);


       when(req.getParameter("input")).thenReturn("123");

        servlet.doGet(req, resp);

        //verify(request, times(1)).getParameter("input");


    }

}